﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Events {
	
	public class EventManager {

		#region Events/Delegates
		public delegate void StartGameAction();
		public static event StartGameAction startGameEvent;

        public delegate void JoinLeaveGameAction (int playerNumber, bool isJoining);
        public static event JoinLeaveGameAction joinLeaveGameEvent;

        public delegate void SetupCameraAction (int playerNumber, int playerPosition, int numPlayers);
        public static event SetupCameraAction setupCameraEvent;

        public delegate void EndStateAction ();
        public static event EndStateAction endAttackStateEvent;
        public static event EndStateAction endStumblingStateEvent;
        public static event EndStateAction endStunnedStateEvent;
        public static event EndStateAction endShoveStateEvent;
        #endregion

        #region Event-Calling Methods
        public static void StartGame () {
			if (startGameEvent != null) {
				startGameEvent ();
			}	
		}

        public static void JoinLeaveGame (int playerNumber, bool isJoining) {
            if(joinLeaveGameEvent != null) {
                joinLeaveGameEvent (playerNumber, isJoining);
            }
        }

        public static void SetupCamera (int playerNumber, int playerPosition, int numPlayers) {
            if(setupCameraEvent != null) {
                setupCameraEvent (playerNumber, playerPosition, numPlayers);
            }
        }

        public static void EndAttackState () {
            if(endAttackStateEvent != null) {
                endAttackStateEvent ();
            }
        }

        public static void EndStumblingState () {
            if(endStumblingStateEvent != null) {
                endStumblingStateEvent ();
            }
        }

        public static void EndStunnedState() {
            if(endStumblingStateEvent != null) {
                endStunnedStateEvent ();
            }
        }

        public static void EndShovingState () {
            if (endShoveStateEvent != null) {
                endShoveStateEvent ();
            }
        }
		#endregion
	}
	
}

