﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;
using UnityEngine.SceneManagement;

namespace Scripts.UI {
	
	public class MainMenuController : MonoBehaviour {
		
		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public void StartGame () {
            //EventManager.StartGame ();
            SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
		}

		public void Options () {
			
		}

		public void QuitGame () {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#else
			Application.Quit();
			#endif
		}
	}
}
