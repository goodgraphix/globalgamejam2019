﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDeadState : HomeezDeadState {

    private AI AI;

    // Start is called before the first frame update
    void Awake() {
        AI = GetComponent<AI> ();
    }


    // Update is called once per frame
    void Update() {
        //do what you need to for the state

        //When you leave this state, set next state
        //AI.SetNextState (AIStates.ATTACKING);
        //this.enabled = false;
    }

    private void OnEnable () {
        //Works as an enter state function
        AI.spawnDaddy.decreasePopulation();
    }

    private void OnDisable () {
        //Works as an exit state function

    }
}
