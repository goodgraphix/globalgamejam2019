﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Homeez {

    private float baseSuccess;
    private float failBonus = 0f;
    private float failModifier;
    private float passiveActionThresh;
    private float actionRange;

    private Homeez targetPlayer;

    private AIFleeingState fleeingState;
    private AIAdvancingState advancingState;
    public Spawner spawnDaddy;

    // Start is called before the first frame update
    void Start() {
        //do this for every state component
        commonStart();
    }

    void Update() {
        if(targetPlayer != null) {
            homeTform.LookAt(targetPlayer.Tform);
        }

        if(this.nextState != this.currentState) {
            this.currentState = this.nextState;
            SwitchStates();
        }
    }

    //local setup method. Extending class will not have hAxis and vAxis
    protected override void localStart() {
        EnemySettings enemyConfig = (EnemySettings) config;
        health = Random.Range(enemyConfig.healthMin, enemyConfig.healthMax);
        baseSuccess = enemyConfig.baseSuccess;
        failModifier = enemyConfig.failBonus;
        passiveActionThresh = enemyConfig.passiveActionThresh;
        actionRange = enemyConfig.actionRange;

        fleeingState = GetComponent<AIFleeingState>();
        advancingState = GetComponent<AIAdvancingState>();
    }

    public void targetNearestPlayer() {
        //find nearest player
        targetPlayer = PlayerManager.pm.getNearestPlayer(tform);
    }

    public bool areWeInRange() {
        //logic to check distance between target and self
        return (targetPlayer != null && Vector3.Distance(targetPlayer.Tform.position, tform.position) <= actionRange);
    }

    public Homeez getTargetPlayer() {
        return targetPlayer;
    }

    public void drawNearToTarget() {
        homeTform.LookAt(targetPlayer.Tform);
    }

    public HomeezStates assessTarget() {
        Debug.Log("Asses the target!");
        HomeezStates newState;
        //if target dead or missing, let's just do nothing
        if(targetPlayer == null || targetPlayer.CurrentState == HomeezStates.DEAD) {
            newState = HomeezStates.NORMAL;
        } else {
            //otherwise, let's roll for success
            if(rollForSuccess()) {
                Debug.Log("Success! Do the best thing!");
                newState = getCorrectAction(targetPlayer.CurrentState);
            } else {
                Debug.Log("Fail! Do Some random thing!");
                newState = getRandomAction();
            }
        }
        return newState;
    }

    private HomeezStates getCorrectAction(HomeezStates targetState) {
        bool doPassive = rollForPassive();
        Debug.Log("Should I be cautious? " + (doPassive ? "yeah, I should." : "NAH. Get'em"));
        switch(targetState) {
            case HomeezStates.NORMAL:
                return doPassive ? HomeezStates.SHOVING : HomeezStates.ATTACKING;
            case HomeezStates.ATTACKING:
                return doPassive ? HomeezStates.FLEEING : HomeezStates.BLOCKING;
            case HomeezStates.BLOCKING:
                return HomeezStates.SHOVING;
            case HomeezStates.SHOVING:
                return doPassive ? HomeezStates.FLEEING : HomeezStates.ATTACKING;
            case HomeezStates.STUMBLING:
                return HomeezStates.ATTACKING;
            case HomeezStates.STUNNED:
                return HomeezStates.ATTACKING;
            default:
                return HomeezStates.NORMAL;
        }
    }

    private HomeezStates getRandomAction() {
        int actionIndex = Random.Range(0, 5);
        switch(actionIndex) {
            case 0:
                return HomeezStates.FLEEING;
            case 1:
                return HomeezStates.ATTACKING;
            case 2:
                return HomeezStates.BLOCKING;
            case 3:
                return HomeezStates.SHOVING;
            default:
                return HomeezStates.NORMAL;
        }
    }

    private bool rollForSuccess() {
        bool success = Random.Range(0f, 1f) < (baseSuccess + failBonus);
        if(success) {
            failBonus = 0f;
        } else {
            failBonus += failModifier;
        }
        return success;
    }

    private bool rollForPassive() {
        return Random.Range(0f, 1f) < passiveActionThresh;
    }

    protected override void SwitchStates() {
        //catch UI-only states, then test remaining cases in base implementation
        switch(this.currentState) {
            case HomeezStates.FLEEING:
                fleeingState.enabled = true;
                break;
            case HomeezStates.ADVANCING:
                advancingState.enabled = true;
                break;
            default:
                base.SwitchStates();
                break;
        }
    }

    

    public override void EndAttackState() {
        attackingState.enabled = false;
        SetNextState(HomeezStates.NORMAL);
    }

    public override void EndShoveState() {
        shovingState.enabled = false;
        SetNextState(HomeezStates.NORMAL);
    }

    private void DeactiveAIState() {
        switch(currentState) {
            case HomeezStates.NORMAL:
                normalState.enabled = false;
                break;
            case HomeezStates.ADVANCING:
                advancingState.enabled = false;
                break;
            case HomeezStates.ATTACKING:
                attackingState.enabled = false;
                break;
            case HomeezStates.BLOCKING:
                blockingState.enabled = false;
                break;
            case HomeezStates.FLEEING:
                fleeingState.enabled = false;
                break;
            case HomeezStates.SHOVING:
                shovingState.enabled = false;
                break;
            case HomeezStates.STUMBLING:
                stumblingState.enabled = false;
                break;
            case HomeezStates.STUNNED:
                stunnedState.enabled = false;
                break;
            default:
                break;
        }
    }

}
