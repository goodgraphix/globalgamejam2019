﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    //this spawner's transform
    private Transform tform;
    private bool spawnerRunning = false;

    public AI enemyClass;
    public EnemySettings baddySettings;
    public SpawnerSettings settings;

    //tracking property for the # of baddies that this spawner spawns EVER.
    public int totalBaddiesSpawned = 0;

    //tracking property for the CURRENT # of instances for this baddy
    private int currentPopulation = 0;

    // Use this for initialization
    void Start() {
        tform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update() {
        if(canKeepSpawning() && PlayerManager.pm.checkIfPlayerNear(tform, settings.radius)) {
            if(!spawnerRunning) {
                enableSpawner();
            }
        } else {
            //nobody in range or out of baddies to spawn
            disableSpawner();
        }
    }

    private bool canKeepSpawning() {
        return currentPopulation < settings.populationLimit && (settings.totalSpawnLimit == 0 || totalBaddiesSpawned < settings.totalSpawnLimit);
    }

    private void enableSpawner() {
        spawnerRunning = true;
        StartCoroutine(releaseTheBaddies());
    }

    private void disableSpawner() {
        spawnerRunning = false;
    }

    private int getSpawnAmount() {
        int[] compareMins;
        int[] compareMaxes;

        int unitsUntilLimit = settings.populationLimit - currentPopulation;
        //check whether we are allowed to make enemies forever
        if(settings.totalSpawnLimit == 0) {
            compareMins = new int[] { settings.minSpawn, unitsUntilLimit };
            compareMaxes = new int[] { settings.maxSpawn, unitsUntilLimit };
        } else {
            int remainingSpawns = settings.totalSpawnLimit - totalBaddiesSpawned;
            compareMins = new int[] { settings.minSpawn, unitsUntilLimit, remainingSpawns };
            compareMaxes = new int[] { settings.maxSpawn, unitsUntilLimit, remainingSpawns };
        }

        int safeMin = Mathf.Min(compareMins);
        int safeMax = Mathf.Min(compareMaxes);
        return Random.Range(safeMin, safeMax + 1);
    }

    IEnumerator releaseTheBaddies() {
        while(spawnerRunning) {
            int baddiesToSpawn = getSpawnAmount();
            for(int i = 0; i < baddiesToSpawn; i++) {
                spawnBaddy();
            }
            yield return new WaitForSeconds(settings.spawnDelay);
        }
    }

    private void spawnBaddy() {
        AI newBaddy = Instantiate(enemyClass, tform.position, tform.rotation);
        newBaddy.config = baddySettings;
        newBaddy.spawnDaddy = this;
        currentPopulation++;
    }

    public void decreasePopulation() {
        currentPopulation--;
    }
}
