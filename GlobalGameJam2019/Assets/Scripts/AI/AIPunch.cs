﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPunch : MonoBehaviour {
    public AudioClip[] punchSounds;

    protected AudioSource audioSource;

    public Homeez homeez;

    // Start is called before the first frame update
    void Start () {
        audioSource = GetComponent<AudioSource> ();
    }

    private void OnTriggerEnter (Collider other) {
        if (other.CompareTag ("Player")) {
            Homeez player = other.transform.root.GetComponent<Homeez> ();
            player.ReceiveAction (homeez);
            PlayPunchSound ();
        }
    }

    private void PlayPunchSound () {
        if (punchSounds.Length == 0) {
            return;
        }

        int soundIndex = Random.Range (0, punchSounds.Length);
        if (audioSource.isPlaying) {
            audioSource.Stop ();
            audioSource.PlayOneShot (punchSounds[soundIndex]);
        } else {
            audioSource.PlayOneShot (punchSounds[soundIndex]);
        }
    }
}

