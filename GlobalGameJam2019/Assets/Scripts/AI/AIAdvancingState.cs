﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAdvancingState : HomeezAttackingState {

    private AI AI;

    // Start is called before the first frame update
    void Awake() {
        AI = GetComponent<AI>();
    }

    // Update is called once per frame
    void Update() {
        //do what you need to for the state
        AI.targetNearestPlayer();
        HomeezStates nextState;
        if(AI.areWeInRange()) {
            nextState = AI.assessTarget();
            //if the next state is NORMAL, we do no need to leave and re-enter NORMAL
            Debug.Log("Next state: " + nextState);
            Debug.Log("Current state: " + AI.CurrentState);
            if(nextState != AI.CurrentState) {
                AI.SetNextState(nextState);
                this.enabled = false;
            }
        } else {
            moveTowardsTarget();
        }
    }

    private void moveTowardsTarget() {
        Vector3 delta = (AI.getTargetPlayer().Tform.position - AI.Tform.position).normalized;
        //same stuff as player movement
        Vector3 moveForce = new Vector3(delta.x * Time.deltaTime * AI.speedMultiplier, 0f, delta.z * Time.deltaTime * AI.speedMultiplier);
        AI.RBody.velocity = moveForce;
    }

    private void OnEnable() {
        Debug.Log("CHARGE!");
        //Works as an enter state function
    }

    private void OnDisable() {
        //Works as an exit state function

    }
}
