﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBlockingState : HomeezBlockingState {

    private AI AI;

    // Start is called AI the first frame update
    void Awake() {
        AI = GetComponent<AI> ();
    }


    // Update is called once per frame
    void Update() {

    }

    private void OnEnable () {
        Debug.Log("BLOCK");
        //Works as an enter state function
        AI.animator.SetInteger ("currentState", 1);
        float randomBlockTime = Random.Range(0.5f, 2.5f);
        StartCoroutine(doABlock(randomBlockTime));
    }

    IEnumerator doABlock(float duration) {
        new WaitForSeconds(duration);
        stopBlocking();
        yield return null;
    }

    private void stopBlocking() {
        enabled = false;
        AI.SetNextState(HomeezStates.NORMAL);
    }

    private void OnDisable () {
        //Works as an exit state function

    }
}
