﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAttackingState : HomeezAttackingState {

    private AI AI;

    // Start is called before the first frame update
    void Awake() {
        AI = GetComponent<AI> ();
    }


    // Update is called once per frame
    void Update() {

    }

    private void OnEnable () {
        //Works as an enter state function
        AI.animator.SetInteger("currentState", 2);
    }

    private void OnDisable () {
        //Works as an exit state function

    }
}
