﻿using System.Collections.Generic;
using Scripts.Events;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager pm;

    public Homeez[] allHomeez;
    public HashSet<Homeez> currentHomeez = new HashSet<Homeez>();

    private void Awake() {
        if (pm == null) {
            pm = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start() {
        //start with player 1 homie (it had better be first in array!!!)
        currentHomeez.Add(allHomeez[0]);
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnEnable() {
        EventManager.joinLeaveGameEvent += JoinLeaveGame;
    }

    private void OnDisable() {
        EventManager.joinLeaveGameEvent -= JoinLeaveGame;
    }

    public void JoinLeaveGame(int playerNumber, bool isJoining) {
        foreach(Homeez iHomee in allHomeez) { 
            if (playerNumber == iHomee.playerNumber) {
                if(isJoining) {
                    currentHomeez.Add(iHomee);
                } else {
                    currentHomeez.Remove(iHomee);
                }
                return;
            }
        }        
    }

    public Homeez getNearestPlayer(Transform otherPosition) {
        float closeDelta = float.MaxValue;
        Homeez closeHomee = null;
        foreach(Homeez iHomee in currentHomeez) {
            float iDelta = Vector3.Distance(iHomee.Tform.position, otherPosition.position);
            if (iDelta < closeDelta) {
                closeDelta = iDelta;
                closeHomee = iHomee;
            }
        }
        return closeHomee;
    }

    public bool checkIfPlayerNear(Transform otherPosition, float testDistance) {
        foreach(Homeez iHomee in currentHomeez) {
            if (Vector3.Distance(iHomee.Tform.position, otherPosition.position) <= testDistance) {
                return true;
            }
        }
        return false;
    }
}
