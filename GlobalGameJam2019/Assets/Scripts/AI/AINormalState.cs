﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AINormalState : HomeezNormalState {

    private AI AI;

    // Start is called before the first frame update
    void Awake() {
        AI = GetComponent<AI> ();
    }

    // Update is called once per frame
    void Update() {
        //do what you need to for the state
        AI.targetNearestPlayer();
        HomeezStates nextState;
        if(AI.areWeInRange()) {
            nextState = AI.assessTarget();
            //if the next state is NORMAL, we do no need to leave and re-enter NORMAL
            Debug.Log("Next state: " + nextState);
            Debug.Log("Current state: " + AI.CurrentState);
        } else {
            nextState = HomeezStates.ADVANCING;
        }
        if(nextState != AI.CurrentState) {
            AI.SetNextState(nextState);
            this.enabled = false;
        }
    }

    private void OnEnable () {
        //Works as an enter state function
    }

    private void OnDisable () {
        //Works as an exit state function
    }
}
