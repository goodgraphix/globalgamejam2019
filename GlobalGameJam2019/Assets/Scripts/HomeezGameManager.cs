﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;

public class HomeezGameManager : MonoBehaviour
{

    //the list of active players
    List<int> activePlayers;

    // Start is called before the first frame update
    void Start() {
        activePlayers = new List<int> ();
        activePlayers.Add (1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable () {
        EventManager.joinLeaveGameEvent += JoinLeaveGame;
    }

    private void OnDisable () {
        EventManager.joinLeaveGameEvent -= JoinLeaveGame;
    }

    private void JoinLeaveGame (int playerNumber, bool isJoining) {
        if(isJoining && activePlayers.Count < 4) {
            activePlayers.Add (playerNumber);
        } else {
            activePlayers.Remove (playerNumber);
        }
        foreach (int player in activePlayers) {
            int playerPosition = activePlayers.IndexOf (player) + 1;
            EventManager.SetupCamera (player, playerPosition, activePlayers.Count);
        }
    }
}
