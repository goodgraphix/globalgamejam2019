﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;

public class TriggerEndStates : MonoBehaviour
{

    private Homeez homeez;

    // Start is called before the first frame update
    void Awake() {
        homeez = transform.parent.parent.gameObject.GetComponent<Homeez>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void EndAttackState() {
        homeez.EndAttackState();
    }

    public void EndShoveState () {
        homeez.EndShoveState();
    }

    public void EndStumblingState () {
        homeez.EndStumblingState();
    }

    public void EndStunnedState() {
        homeez.EndStunnedState();
    }
}
