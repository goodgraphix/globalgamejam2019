﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;

public class Homeez : MonoBehaviour {
    //cheaper way of keeping track of states
    protected HomeezStates currentState;
    public HomeezStates CurrentState { get => currentState; set => currentState = value; }

    protected HomeezStates nextState;

    //Actual states to manage, all of them should be here
    protected HomeezNormalState normalState;
    protected HomeezAttackingState attackingState;
    protected HomeezBlockingState blockingState;
    protected HomeezStumblingState stumblingState;
    protected HomeezStunnedState stunnedState;
    protected HomeezShovingState shovingState;
    protected HomeezDeadState deadState;

    //the rendered object for this player
    public GameObject home;

    //this player's ID number
    public int playerNumber;
    public HomeezSettings config;

    //this player's input strings
    private string hAxis;
    private string vAxis;
    private string startButton;
    private string yButton;
    private string attackButton;
    private string blockButton;
    private string shoveButton;

    public string HAxis { get => hAxis; }
    public string VAxis { get => vAxis; }
    public string AttackButton { get => attackButton; }
    public string BlockButton { get => blockButton; }
    public string ShoveButton { get => shoveButton; }

    //this player's camera
    private Camera myCamera;

    //this player's rigidbody
    protected Rigidbody rBody;
    public Rigidbody RBody { get => rBody; set => rBody = value; }

    //this player's transform
    protected Transform tform;
    public Transform Tform { get => tform; set => tform = value; }
    public Transform homeTform;

    public float speedMultiplier;
    private float prevAngle;

    //is this player active in the game?
    private bool isPlayerActive;

    protected float health;

    protected HomeezSound homeezSound;
    public Animator animator;


    // Start is called before the first frame update
    void Start() {
        //do this for every state component
        commonStart();
    }

    protected void commonStart() {
        normalState = GetComponent<HomeezNormalState>();
        attackingState = GetComponent<HomeezAttackingState>();
        blockingState = GetComponent<HomeezBlockingState>();
        stumblingState = GetComponent<HomeezStumblingState>();
        stunnedState = GetComponent<HomeezStunnedState>();
        shovingState = GetComponent<HomeezShovingState>();
        deadState = GetComponent<HomeezDeadState>();
        myCamera = GetComponentInChildren<Camera>();
        rBody = GetComponent<Rigidbody>();
        tform = GetComponent<Transform>();
        //homeTform = home.GetComponentInParent<Transform>();
        homeezSound = GetComponentInChildren<HomeezSound> ();
        prevAngle = 0f;
        if(playerNumber == 1) {
            normalState.enabled = true;
            isPlayerActive = true;
        } else {
            isPlayerActive = false;
        }
        currentState = HomeezStates.NORMAL;
        localStart();
    }

    //local setup method. Extending class will not have hAxis and vAxis
    protected virtual void localStart() {
        health = config.healthMax;
        hAxis = "Horizontal" + playerNumber;
        vAxis = "Vertical" + playerNumber;
        startButton = "Start" + playerNumber;
        yButton = "YButton" + playerNumber; //Y
        attackButton = "Attack" + playerNumber; //B
        blockButton = "Block" + playerNumber; //A
        shoveButton = "Shove" + playerNumber; //X
    }

    // Update is called once per frame
    void Update() {
        //check for player joins/leaves
        if(Input.GetButtonDown(startButton) && playerNumber != 1) {
            if(!this.isPlayerActive) {
                this.isPlayerActive = true;
                home.SetActive(true);
                //activate normal state, start there
                rBody.useGravity = true;
                normalState.enabled = true;
                EventManager.JoinLeaveGame(this.playerNumber, this.isPlayerActive);
            } else {
                this.isPlayerActive = false;
                //deactivate the active state
                myCamera.enabled = false;
                rBody.useGravity = false;
                DeactivateStates();
                home.SetActive(false);
                EventManager.JoinLeaveGame(this.playerNumber, this.isPlayerActive);
            }
        }

        if(this.isPlayerActive) {
            if (Input.GetButtonDown(yButton)) {
                homeezSound.PlayHomeezSound ();
            }

            if(this.nextState != this.currentState) {
                this.currentState = this.nextState;
                SwitchStates();
            }
        }

        float xAxis = Input.GetAxis(hAxis);
        float yAxis = Input.GetAxis(vAxis);
        Vector3 moveForce = new Vector3(xAxis * Time.deltaTime * speedMultiplier, 0f, -1 * yAxis * Time.deltaTime * speedMultiplier);
        rBody.velocity = moveForce;
        Vector3 v1 = new Vector3(xAxis, 0, yAxis);
        float angle = Vector3.Angle(Vector3.back, v1);
        if(xAxis < 0) {
            angle = angle * -1;
        }
        if (Mathf.Approximately (xAxis, 0f) && Mathf.Approximately (yAxis, 0f)) {
            homeTform.rotation = Quaternion.Euler (new Vector3 (0, prevAngle, 0));
        } else {
            homeTform.rotation = Quaternion.Euler (new Vector3 (0, angle, 0));
            prevAngle = angle;
        }
    }

    private void OnEnable() {
        EventManager.setupCameraEvent += SetupCamera;
    }

    private void OnDisable() {
        EventManager.setupCameraEvent -= SetupCamera;
    }

    protected virtual void SwitchStates() {
        switch(this.currentState) {
            case HomeezStates.NORMAL:
                normalState.enabled = true;
                break;
            case HomeezStates.ATTACKING:
                attackingState.enabled = true;
                break;
            case HomeezStates.BLOCKING:
                blockingState.enabled = true;
                break;
            case HomeezStates.DEAD:
                deadState.enabled = true;
                break;
            case HomeezStates.SHOVING:
                shovingState.enabled = true;
                break;
            case HomeezStates.STUMBLING:
                stumblingState.enabled = true;
                break;
            case HomeezStates.STUNNED:
                stunnedState.enabled = true;
                break;
            default:
                break;
        }
    }

    public void SetNextState(HomeezStates newState) {
        this.nextState = newState;
    }

    public HomeezStates GetNextState() {
        return this.nextState;
    }

    private void DeactivateStates() {
        switch(currentState) {
            case HomeezStates.NORMAL:
                normalState.enabled = false;
                break;
            default:
                break;
        }
        this.currentState = HomeezStates.NORMAL;
        this.nextState = HomeezStates.NORMAL;
    }

    private void SetupCamera(int playerNumber, int playerPosition, int numPlayers) {
        if(playerNumber == this.playerNumber) {
            if(numPlayers == 1) {
                myCamera.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
            } else if(numPlayers == 2) {
                if(playerPosition == 1) {
                    myCamera.rect = new Rect(0.0f, 0.0f, 0.5f, 1.0f);
                } else {
                    myCamera.rect = new Rect(0.5f, 0.0f, 0.5f, 1.0f);
                }
            } else if(numPlayers == 3) {
                if(playerPosition == 1) {
                    myCamera.rect = new Rect(0.0f, 0.0f, 0.5f, 1.0f);
                } else if(playerPosition == 2) {
                    myCamera.rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
                } else {
                    myCamera.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                }
            } else if(numPlayers == 4) {
                if(playerPosition == 1) {
                    myCamera.rect = new Rect(0.0f, 0.0f, 0.5f, 0.5f);
                } else if(playerPosition == 2) {
                    myCamera.rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
                } else if(playerPosition == 3) {
                    myCamera.rect = new Rect(0.0f, 0.5f, 0.5f, 0.5f);
                } else {
                    myCamera.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                }
            }
            myCamera.enabled = true;
        }
    }

    public virtual void EndAttackState() {
        attackingState.enabled = false;
        SetNextState(Input.GetButton(blockButton) ? HomeezStates.BLOCKING : HomeezStates.NORMAL);
    }

    public virtual void EndShoveState() {
        shovingState.enabled = false;
        SetNextState(Input.GetButton(blockButton) ? HomeezStates.BLOCKING : HomeezStates.NORMAL);
    }

    public void EndStumblingState() {
        stumblingState.enabled = false;
        SetNextState(HomeezStates.NORMAL);
    }

    public void EndStunnedState() {
        stunnedState.enabled = false;
        SetNextState(HomeezStates.NORMAL);
    }

    public void StunHomeez () {
        attackingState.enabled = false;
        SetNextState (HomeezStates.STUNNED);
    }

    public void StumbleHomeez() {
        shovingState.enabled = false;
        SetNextState(HomeezStates.STUMBLING);
    }

    private void DeactivateHomeezState () {
        switch (currentState) {
            case HomeezStates.NORMAL:
                normalState.enabled = false;
                break;
            case HomeezStates.ATTACKING:
                attackingState.enabled = false;
                break;
            case HomeezStates.BLOCKING:
                blockingState.enabled = false;
                break;
            case HomeezStates.SHOVING:
                shovingState.enabled = false;
                break;
            case HomeezStates.STUMBLING:
                stumblingState.enabled = false;
                break;
            case HomeezStates.STUNNED:
                stunnedState.enabled = false;
                break;
            default:
                break;
        }
    }

    public void ReceiveAction (Homeez homeez) {
        if (homeez.CurrentState == HomeezStates.ATTACKING) {
            if (currentState == HomeezStates.BLOCKING) {
                //tell him he is stunned
                homeez.StunHomeez ();
            } else {
                //take damage and check for death
                DeactivateHomeezState ();
                this.SetNextState (HomeezStates.STUNNED);

            }
        } else if (homeez.CurrentState == HomeezStates.SHOVING) {
            if (currentState == HomeezStates.ATTACKING) {
                //tell him he is stumbling
                homeez.StumbleHomeez ();
            } else {
                //I am stumbling
                DeactivateHomeezState ();
                this.SetNextState (HomeezStates.STUMBLING);
            }
        }
    }
}
