﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeezDeadState : MonoBehaviour
{

    private Homeez homeez;

    // Start is called before the first frame update
    void Awake() {
        homeez = GetComponent<Homeez> ();
    }


    // Update is called once per frame
    void Update() {
        //do what you need to for the state

        //When you leave this state, set next state
        //homeez.SetNextState (HomeezStates.ATTACKING);
        //this.enabled = false;
    }

    private void OnEnable () {
        //Works as an enter state function

    }

    private void OnDisable () {
        //Works as an exit state function

    }
}
