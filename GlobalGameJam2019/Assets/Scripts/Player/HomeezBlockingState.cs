﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;

public class HomeezBlockingState : MonoBehaviour
{

    private Homeez homeez;

    // Start is called before the first frame update
    void Awake() {
        homeez = GetComponent<Homeez> ();
    }


    // Update is called once per frame
    void Update() {
        //do what you need to for the state

        if(Input.GetButtonDown(homeez.AttackButton)) {
            this.enabled = false;
            homeez.SetNextState(HomeezStates.ATTACKING);
        } else if(Input.GetButtonDown(homeez.ShoveButton)) {
            this.enabled = false;
            homeez.SetNextState(HomeezStates.SHOVING);
        } else if(!Input.GetButton(homeez.BlockButton)) {
            this.enabled = false;
            homeez.SetNextState(HomeezStates.NORMAL);
        }
    }

    private void OnEnable () {
        //Works as an enter state function
        homeez.animator.SetInteger ("currentState", 1);
    }

    private void OnDisable () {
        //Works as an exit state function

    }
}
