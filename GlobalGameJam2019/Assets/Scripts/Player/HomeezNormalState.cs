﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeezNormalState : MonoBehaviour
{

    private Homeez homeez;

    // Start is called before the first frame update
    void Awake() {
        homeez = GetComponent<Homeez> ();
    }

    // Update is called once per frame
    void Update() {

        if(Input.GetButtonDown(homeez.AttackButton)) {
            this.enabled = false;
            homeez.SetNextState (HomeezStates.ATTACKING);
        } else if (Input.GetButtonDown(homeez.ShoveButton)) {
            this.enabled = false;
            homeez.SetNextState(HomeezStates.SHOVING);
        } else if (Input.GetButton(homeez.BlockButton)) {
            this.enabled = false;
            homeez.SetNextState(HomeezStates.BLOCKING);
        }
        //float xAxis = Input.GetAxis (hAxis);
        //float yAxis = Input.GetAxis (vAxis);
        //Vector3 moveForce = new Vector3 (xAxis * Time.deltaTime * speedMultiplier, 0f, -1 * yAxis * Time.deltaTime * speedMultiplier);
        //rBody.velocity = moveForce;
        //Vector3 v1 = new Vector3 (xAxis, 0, yAxis);
        //float angle = Vector3.Angle (Vector3.back, v1);
        //if (xAxis < 0) {
        //    angle = angle * -1;
        //}
        //if (Mathf.Approximately (xAxis, 0f) && Mathf.Approximately (yAxis, 0f)) {
        //    homeTform.rotation = Quaternion.Euler (new Vector3 (0, prevAngle, 0));
        //} else {
        //    homeTform.rotation = Quaternion.Euler (new Vector3 (0, angle, 0));
        //    prevAngle = angle;
        //}
        //do what you need to for the state

        //ignore moved

        //When you leave this state, set next state
        //homeez.SetNextState (HomeezStates.ATTACKING);
        //this.enabled = false;
    }

    private void OnEnable () {
        //Works as an enter state function
        homeez.animator.SetInteger ("currentState", 0);
    }

    private void OnDisable () {
        //Works as an exit state function
    }
}
