﻿public enum HomeezStates {
    //COMMOON STATES
    NORMAL,
    ATTACKING,
    BLOCKING,
    STUMBLING,
    STUNNED,
    SHOVING,
    DEAD,
    //UI ONLY STATES
    FLEEING,
    ADVANCING
}

