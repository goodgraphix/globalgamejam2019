﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Events;

public class HomeezStunnedState : MonoBehaviour
{

    private Homeez homeez;

    // Start is called before the first frame update
    void Awake() {
        homeez = GetComponent<Homeez> ();
    }


    // Update is called once per frame
    void Update() {
        //do what you need to for the state
    }

    private void OnEnable () {
        //Works as an enter state function
        homeez.animator.SetInteger ("currentState", 4);
    }

    private void OnDisable () {
        //Works as an exit state function
    }
}
