﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeezSound : MonoBehaviour
{
    public AudioClip[] homeezSounds;

    protected AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource> ();
    }

    public void PlayHomeezSound () {
        if(homeezSounds.Length == 0) {
            return;
        }

        int soundIndex = Random.Range (0, homeezSounds.Length);
        if(!audioSource.isPlaying) {
            audioSource.PlayOneShot (homeezSounds[soundIndex]);
        }
    }
}
