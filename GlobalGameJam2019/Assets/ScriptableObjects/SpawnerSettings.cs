﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Spawner Settings", menuName = "Homeez Scriptables/Spawner Settings", order = 3)]
public class SpawnerSettings : ScriptableObject {
    public int minSpawn = 1;
    public int maxSpawn = 3;
    public float radius = 10f;
    public float spawnDelay = 5f;
    public int totalSpawnLimit = 5; // leave as 0 to allow infinte spawns
    public int populationLimit = 3;
}