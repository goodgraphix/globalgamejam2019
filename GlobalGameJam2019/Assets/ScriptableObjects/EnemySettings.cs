﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Settings", menuName = "Homeez Scriptables/Enemy Settings", order = 2)]
public class EnemySettings : HomeezSettings {
    public float healthMin = 2f;
    public float baseSuccess = 0f;
    public float failBonus = 0.01f;
    public float passiveActionThresh = 0.33f;
    public float actionRange = 5f;
}
