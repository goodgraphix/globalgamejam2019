﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Home Settings", menuName = "Homeez Scriptables/Home Settings", order = 1)]
public class HomeezSettings : ScriptableObject {
    public float healthMax = 5f;
    public float attackMin = 1f;
    public float attackMax = 3f;
    public float bigAttackMod = 2f;
    public float blockValue = 3f;
}
